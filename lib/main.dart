import 'package:car_speed_performer/car_speed_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Car Speed Performer',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        canvasColor: Colors.white
      ),
      home: const MyHomePage(title: 'Car Speed Performer Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool switchStatus = false;

  late CarSpeedViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _viewModel = CarSpeedViewModel(60);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ChangeNotifierProvider(
        create: (BuildContext context) => _viewModel,
        child: Consumer<CarSpeedViewModel> (
          builder: (context, model, child) {
            return Column (
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    const SizedBox(height: 30,),
                    Image.asset(
                      "assets/images/car_icon.png",
                      width: 54,
                      color: _viewModel.carModeColor,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("1", style: TextStyle(
                            color: _viewModel.blueCarColor,
                            fontSize: 28,
                            fontWeight: FontWeight.bold
                        ),),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: SizedBox(
                              width: 120,
                              height: 50,
                              child: FittedBox(
                                fit: BoxFit.fill,
                                child: CupertinoSwitch(
                                  activeColor: Colors.grey,
                                  trackColor: Colors.grey.withOpacity(0.4),
                                  thumbColor: Colors.white,
                                  // boolean variable value
                                  value: switchStatus,
                                  // changes the state of the switch
                                  onChanged: (value) {
                                    switchStatus = value;
                                    _viewModel.modifySpeed(switchStatus ? 120 : 60);
                                  },
                                ),
                              )
                          ),
                        ),
                        Text("2", style: TextStyle(
                            color: _viewModel.yellowCarColor,
                            fontSize: 28,
                            fontWeight: FontWeight.bold
                        ),),
                      ],
                    ),
                  ],
                ),
                Column(
                  children: [
                    Container(
                      height: 220,
                      width: 220,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(500),
                          border: Border.all(width: 20, color: _viewModel.alertColor)),
                      child: Card(
                        color: Colors.white,
                        elevation: _viewModel.highSpeed ? 0 : 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(500.0),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(_viewModel.speed.toString(), style: const TextStyle(
                                    fontSize: 64,
                                    fontWeight: FontWeight.bold
                                ), textAlign: TextAlign.center,),
                              const Text("km/h", style: TextStyle(
                                  fontSize: 20
                              ),),
                            ],
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 10,),
                    const Text("Current speed",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20
                      ),
                    ),
                  ],
                ),
                Container(
                  color: _viewModel.carModeColor,
                  height: 75,
                )
              ],
            );
          }
        )
      )
    );
  }
}
