import 'package:flutter/material.dart';

class CarSpeedViewModel extends ChangeNotifier {

  int speed = 0;

  bool get highSpeed => speed > 100;

  Color get blueCarColor => const Color(0xFF0078D4);

  Color get yellowCarColor => const Color(0xFFFFC400);

  Color get alertColor => highSpeed ? const Color(0xFFD32F2F) : Colors.white;

  Color get carModeColor => highSpeed ? yellowCarColor : blueCarColor;

  CarSpeedViewModel(this.speed);

  void modifySpeed(int currentSpeed) {
    speed = currentSpeed;
    notifyListeners();
  }

}